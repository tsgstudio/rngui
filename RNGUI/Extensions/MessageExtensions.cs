//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// Extensions for <see cref="Message"/>.
	/// </summary>
	public static class MessageExtensions
	{
		/// <summary>
		/// Change the receiver to the specified target if receiver not set.
		/// </summary>
		/// <param name="target">The target message.</param>
		/// <param name="gameObject">The default receiver.</param>
		public static void SetDefaultReceiver (this Message target, GameObject gameObject)
		{
			if (target != null && target.receiver == null)
				target.receiver = gameObject;
		}
	}
}
