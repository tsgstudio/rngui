﻿//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// Extensions for <see cref="UIPanel"/>.
	/// </summary>
	public static class UIPanelExtensions
	{
		/// <summary>
		/// Calculate the center position of the specified <see cref="UIPanel"/> in world coordinates.
		/// </summary>
		/// <returns>Returns the center position in world coordinates.</returns>
		public static Vector3 GetCenterPoint (this UIPanel panel)
		{
			if (panel == null) return Vector3.zero;

			var mClip = panel.clipRange;
			var mTransform = panel.cachedTransform;
			var mRetVal = mTransform.localPosition;

			mRetVal.x += mClip.x;
			mRetVal.y += mClip.y;

			return mTransform.parent.TransformPoint(mRetVal);
		}
	}
}
