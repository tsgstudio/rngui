﻿//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System;

	/// <summary>
	/// Extensions for <see cref="UIEventType"/>.
	/// </summary>
	public static class UIEventExtensions
	{
		/// <summary>
		/// Get the name of the function associated with specified event type.
		/// </summary>
		/// <param name="eventType">The type of the UI event.</param>
		/// <returns>Returns the name of the associated function.</returns>
		public static string GetFunctionName (this UIEventType eventType)
		{
			switch (eventType)
			{
				case UIEventType.None:
					return string.Empty;

				case UIEventType.OnClick:
					return "OnClick";

				case UIEventType.OnDoubleClick:
					return "OnDoubleClick";

				case UIEventType.OnHover:
				case UIEventType.OnHoverTrue:
				case UIEventType.OnHoverFalse:
					return "OnHover";

				case UIEventType.OnPress:
				case UIEventType.OnPressTrue:
				case UIEventType.OnPressFalse:
					return "OnPress";

				case UIEventType.OnSelect:
				case UIEventType.OnSelectTrue:
				case UIEventType.OnSelectFalse:
					return "OnSelect";

				case UIEventType.OnActivate:
				case UIEventType.OnActivateTrue:
				case UIEventType.OnActivateFalse:
					return "OnActivate";

				case UIEventType.OnDrag:
					return "OnDrag";

				default:
					throw new ArgumentOutOfRangeException("eventType", eventType, string.Format("Event Type {0} is not supported.", eventType));
			}
		}
	}
}
