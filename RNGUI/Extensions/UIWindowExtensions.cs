//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// Extensions for <see cref="UIWindow"/>.
	/// </summary>
	public static class UIWindowExtensions
	{
		/// <summary>
		/// Duplicate the window instance.
		/// </summary>
		/// <typeparam name="T">The type of the window.</typeparam>
		/// <param name="window">The original window instance.</param>
		/// <returns>Returns the duplicate window instance.</returns>
		public static T Duplicate<T> (this T window) where T : UIWindow
		{
			if (window == null) return null;

			var mGameObject = window.gameObject;
			var mTransform = mGameObject.transform;
			var mRootObject = mGameObject.transform.parent;

			var mDuplicate = (GameObject)Object.Instantiate(mGameObject);
			mDuplicate.transform.parent = mRootObject;
			var mWinTransform = mDuplicate.transform;
			mDuplicate.layer = mGameObject.layer;
			mWinTransform.localScale = mTransform.localScale;
			mWinTransform.localPosition = mTransform.localPosition;
			var mWindow = mDuplicate.GetComponent<T>();
			return mWindow;
		}
	}
}
