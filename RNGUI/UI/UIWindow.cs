//----------------------------------------------
//         Copyright � 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	/// <summary>
	/// The actions which can be passed to window.
	/// </summary>
	public enum WindowAction
	{
		/// <summary>
		/// Do nothing.
		/// </summary>
		None,

		/// <summary>
		/// Open the window.
		/// </summary>
		OpenWindow,

		/// <summary>
		/// Close the window.
		/// </summary>
		CloseWindow,

		/// <summary>
		/// Switch the window current state to opposite.
		/// </summary>
		SwitchWindow,

		/// <summary>
		/// Destroy the window.
		/// </summary>
		DestroyWindow,
	}

	/// <summary>
	/// The event handler for window entry selection.
	/// </summary>
	/// <typeparam name="T">The type of the window.</typeparam>
	/// <typeparam name="U">The type of the entry ID.</typeparam>
	/// <param name="window">The source of the event.</param>
	/// <param name="entry">The ID of the selected entry.</param>
	public delegate void WindowOptionEventHandler<T, U> (T window, U entry) where T : UIWindow;

	/// <summary>
	/// The event handler for window action execution.
	/// </summary>
	/// <typeparam name="T">The type of the window.</typeparam>
	/// <param name="window">The source of the event.</param>
	/// <param name="action">The type of the executed action.</param>
	/// <returns></returns>
	public delegate void WindowActionEventHandler<T> (T window, WindowAction action) where T : UIWindow;

	/// <summary>
	/// The UI pop-up window.
	/// </summary>
	/// <remarks>
	/// Note that <see cref="UIWindow"/> component must be enabled on awake in order to find it in static methods.
	/// The disabled window can't add itself to global registry.
	/// </remarks>
	public class UIWindow : MonoBehaviour
	{
		private readonly HashSet<UIWindowEntry> mEntries = new HashSet<UIWindowEntry>();

		#region Static
		private static int mNextIndex = 1;
		private static readonly IList<UIWindow> Windows;

		/// <summary>
		/// Get window by ID.
		/// </summary>
		/// <param name="id">The id of the window.</param>
		/// <returns>The instance of the window if exist; otherwise null.</returns>
		public static UIWindow GetWindow (int id)
		{
			return Windows.FirstOrDefault(x => x.WindowID == id);
		}

		/// <summary>
		/// Get window by name.
		/// </summary>
		/// <param name="windowName">The name of the window.</param>
		/// <returns>The instance of the window if exist; otherwise null.</returns>
		public static UIWindow GetWindow (string windowName)
		{
			return Windows.FirstOrDefault(x => string.Equals(x.WindowName, windowName, StringComparison.InvariantCulture));
		}

		/// <summary>
		/// Open window with specified name.
		/// </summary>
		/// <param name="name">The name of the window.</param>
		/// <returns>The instance of the window if exist; otherwise null.</returns>
		public static UIWindow OpenWindow (string name)
		{
			var mRetVal = GetWindow(name);
			return mRetVal != null ? mRetVal.OpenWindow() : null;
		}

		/// <summary>
		/// Close window with specified name.
		/// </summary>
		/// <param name="name">The name of the window.</param>
		public static UIWindow CloseWindow (string name)
		{
			var mRetVal = GetWindow(name);
			if (mRetVal != null) mRetVal.CloseWindow();
			return mRetVal;
		}

		/// <summary>
		/// Get first window instance of particular type.
		/// </summary>
		/// <typeparam name="T">Type of the window.</typeparam>
		/// <returns>The instance of the window if exist; otherwise null.</returns>
		public static T GetWindow<T> () where T : UIWindow
		{
			return Windows.FirstOrDefault(x => x is T) as T;
		}

		/// <summary>
		/// Get window of particular type by name.
		/// </summary>
		/// <typeparam name="T">The type of the window.</typeparam>
		/// <param name="windowName">The name of the window.</param>
		/// <returns>The instance of the window if exist; otherwise null.</returns>
		public static T GetWindow<T> (string windowName) where T : UIWindow
		{
			return Windows.FirstOrDefault(x => x is T && string.Equals(x.WindowName, windowName, StringComparison.InvariantCulture)) as T;
		}

		/// <summary>
		/// Open first window of particular type.
		/// </summary>
		/// <typeparam name="T">The type of the window.</typeparam>
		/// <returns>The instance of the window if exist; otherwise null.</returns>
		public static T OpenWindow<T> () where T : UIWindow
		{
			var mRetVal = GetWindow<T>();
			return mRetVal != null ? mRetVal.OpenWindow() as T : null;
		}

		/// <summary>
		/// Open all specified windows.
		/// </summary>
		/// <param name="windows">The collection of windows to open.</param>
		public static void OpenWindows (params UIWindow[] windows)
		{
			foreach (var mWindow in windows)
				if (mWindow != null) mWindow.OpenWindow();
		}

		/// <summary>
		/// Close first window of particular type.
		/// </summary>
		/// <typeparam name="T">The type of the window.</typeparam>
		public static T CloseWindow<T> () where T : UIWindow
		{
			var mRetVal = GetWindow<T>();
			if (mRetVal != null) mRetVal.CloseWindow();
			return mRetVal;
		}

		/// <summary>
		/// Close all windows with specified names.
		/// </summary>
		/// <param name="names"></param>
		public static void CloseWindows (params string[] names)
		{
			foreach (var mName in names)
			{
				var mWindow = GetWindow(mName);
				if (mWindow != null) mWindow.CloseWindow();
			}
		}

		/// <summary>
		/// Close all specified windows.
		/// </summary>
		/// <param name="windows">The collection of windows to close.</param>
		public static void CloseWindows (params UIWindow[] windows)
		{
			foreach (var mWindow in windows)
				if (mWindow != null) mWindow.CloseWindow();
		}

		/// <summary>
		/// Close all windows except specified.
		/// </summary>
		/// <param name="except">The collection of the windows to stay open.</param>
		public static void CloseWindowsExcept (params UIWindow[] except)
		{
			var mQuery = Windows.Where(x => x.IsActive).Except(except, new WindowEqualityComparer());
			foreach (var mWindow in mQuery)
				mWindow.CloseWindow();
		}

		/// <summary>
		/// Static constructor.
		/// </summary>
		static UIWindow ()
		{
			Windows = new List<UIWindow>();
		}

		/// <summary>
		/// Get next random window name.
		/// </summary>
		private static string GetNextName ()
		{
			return string.Format("Window {0}", mNextIndex++);
		}

		/// <summary>
		/// Generate unique window name.
		/// </summary>
		private static string GetUniqueName ()
		{
			var mName = GetNextName();

			while (GetWindow(mName) != null)
				mName = GetNextName();

			return mName;
		}

		private class WindowEqualityComparer : IEqualityComparer<UIWindow>
		{
			#region Implementation of IEqualityComparer<UIWindow>
			/// <summary>
			/// Determines whether the specified objects are equal.
			/// </summary>
			/// <returns> true if the specified objects are equal; otherwise, false. </returns>
			/// <param name="x">The first object of type <paramref name="T"/> to compare.</param>
			/// <param name="y">The second object of type <paramref name="T"/> to compare.</param>
			public bool Equals (UIWindow x, UIWindow y)
			{
				return x.WindowName.Equals(y.WindowName);
			}

			/// <summary>
			/// Returns a hash code for the specified object.
			/// </summary>
			/// <returns> A hash code for the specified object. </returns>
			/// <param name="obj">The <see cref="T:System.Object"/> for which a hash code is to be returned.</param>
			/// <exception cref="T:System.ArgumentNullException">The type of <paramref name="obj"/> is a reference type and <paramref name="obj"/> is null.</exception>
			public int GetHashCode (UIWindow obj)
			{
				return obj.WindowName.GetHashCode();
			}
			#endregion
		}
		#endregion

		/// <summary>
		/// This event is raised when window entry selected.
		/// </summary>
		public event WindowOptionEventHandler<UIWindow, int> Selected;

		/// <summary>
		/// This event is raised when window executed some action.
		/// </summary>
		public event WindowActionEventHandler<UIWindow> Executed;

		[SerializeField]
		private string _windowName;
		[SerializeField]
		private GameObject _windowRoot;
		[SerializeField]
		private bool _disableOnStart = true;
		[SerializeField]
		private bool _isDirty;

		/// <summary>
		/// The unique window ID.
		/// </summary>
		public int WindowID { get; private set; }

		/// <summary>
		/// The name of the window.
		/// </summary>
		public string WindowName
		{
			get { return _windowName; }
			set { _windowName = value; }
		}

		/// <summary>
		/// The root object of the window.
		/// </summary>
		public GameObject WindowRoot
		{
			get { return _windowRoot; }
			set { _windowRoot = value; }
		}

		/// <summary>
		/// Whether to disable window after start.
		/// </summary>
		public bool DisableOnStart
		{
			get { return _disableOnStart; }
			set { _disableOnStart = value; }
		}

		/// <summary>
		/// Whether the window is dirty and need to update it's values.
		/// </summary>
		public bool IsDirty
		{
			get { return _isDirty; }
			protected set { _isDirty = value; }
		}

		/// <summary>
		/// Whether the window is currently active.
		/// </summary>
		public bool IsActive
		{
			get { return NGUITools.GetActive(WindowRoot); }
		}

		/// <summary>
		/// Collection of window entries.
		/// </summary>
		public ICollection<UIWindowEntry> Entries
		{
			get { return mEntries; }
		}

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public virtual void Awake ()
		{
			WindowID = GetInstanceID();
			Windows.Add(this);

			if (WindowRoot == null) WindowRoot = gameObject;
			if (WindowName == null) WindowName = name;
		}

		/// <summary>
		/// Start is called just before any of the Update methods is called the first time.
		/// </summary>s
		public virtual void Start ()
		{
			if (string.IsNullOrEmpty(WindowName))
				WindowName = GetUniqueName();

			if (DisableOnStart) CloseWindow();
		}

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		public virtual void OnEnable ()
		{

		}

		/// <summary>
		/// This function is called when the behaviour becomes disabled () or inactive.
		/// </summary>
		public virtual void OnDisable ()
		{
			SetDirty();
		}

		/// <summary>
		/// LateUpdate is called after all Update functions have been called.
		/// </summary>
		public virtual void LateUpdate ()
		{
			if (!IsDirty) return;
			IsDirty = false;
			OnDirty();
		}

		/// <summary>
		/// This function is called when the MonoBehaviour will be destroyed.
		/// </summary>
		public virtual void OnDestroy ()
		{
			Windows.Remove(this);
		}

		/// <summary>
		/// Force window to select entry with specified ID.
		/// </summary>
		/// <param name="entryID">The window entry ID.</param>
		public void SelectEntry (int entryID)
		{
			OnSelected(entryID);
		}

		/// <summary>
		/// Execute specified action towards the window.
		/// </summary>
		/// <param name="action">The action to execute.</param>
		public UIWindow ExecuteAction (WindowAction action)
		{
			switch (action)
			{
				case WindowAction.None:
					break;

				case WindowAction.OpenWindow:
					NGUITools.SetActive(WindowRoot, true);
					OnExecuted(WindowAction.OpenWindow);
					break;

				case WindowAction.CloseWindow:
					NGUITools.SetActive(WindowRoot, false);
					OnExecuted(WindowAction.CloseWindow);
					break;

				case WindowAction.SwitchWindow:
					if (!NGUITools.GetActive(WindowRoot)) OpenWindow(); else CloseWindow();
					OnExecuted(WindowAction.SwitchWindow);
					break;

				case WindowAction.DestroyWindow:
					NGUITools.Destroy(gameObject);
					NGUITools.Destroy(WindowRoot);
					OnExecuted(WindowAction.DestroyWindow);
					break;

				default:
					throw new ArgumentOutOfRangeException("action", "action = " + action);
			}

			return this;
		}

		/// <summary>
		/// Enable the window.
		/// </summary>
		/// <returns>The window instance.</returns>
		public UIWindow OpenWindow ()
		{
			return ExecuteAction(WindowAction.OpenWindow);
		}

		/// <summary>
		/// Disable the window.
		/// </summary>
		/// <returns>The window instance.</returns>
		public UIWindow CloseWindow ()
		{
			return ExecuteAction(WindowAction.CloseWindow);
		}

		/// <summary>
		/// Destroy the window.
		/// </summary>
		public void DestroyWindow ()
		{
			ExecuteAction(WindowAction.DestroyWindow);
		}

		/// <summary>
		/// Switch the window state to the opposite.
		/// </summary>
		/// <returns>The window instance.</returns>
		public UIWindow SwitchWindow ()
		{
			return ExecuteAction(WindowAction.SwitchWindow);
		}

		/// <summary>
		/// Get the window entry with specified ID.
		/// </summary>
		/// <param name="id">The ID of the window entry.</param>
		public UIWindowEntry GetEntry (int id)
		{
			return Entries.FirstOrDefault(x => x.ID == id);
		}

		/// <summary>
		/// Mark window as dirty.
		/// </summary>
		public void SetDirty ()
		{
			IsDirty = true;
		}

		/// <summary>
		/// Attach specified entry to current window.
		/// </summary>
		/// <param name="entry">The entry to attach.</param>
		/// <returns>The handler to detach current entry.</returns>
		public IDisposable Attach (UIWindowEntry entry)
		{
			// Do nothing if already exist
			if (Entries.Contains(entry)) return null;

			Entries.Add(entry);
			entry.Selected += OnEntrySelected;
			return new Detachment(this, entry);
		}

		/// <summary>
		/// This method is called on LateUpdate when window is dirty.
		/// </summary>
		protected virtual void OnDirty ()
		{

		}

		/// <summary>
		/// Callback for entry selection.
		/// </summary>
		private void OnEntrySelected (UIWindowEntry sender)
		{
			OnSelected(sender.ID);
		}

		/// <summary>
		/// Raise <see cref="Selected"/> event.
		/// </summary>
		private void OnSelected (int optionid)
		{
			if (Selected != null)
				Selected(this, optionid);
		}

		/// <summary>
		/// Raise <see cref="Executed"/> event.
		/// </summary>
		private void OnExecuted (WindowAction action)
		{
			if (Executed != null)
				Executed(this, action);
		}

		private class Detachment : IDisposable
		{
			private readonly UIWindow mWindow;
			private readonly UIWindowEntry mEntry;

			public Detachment (UIWindow window, UIWindowEntry entry)
			{
				mWindow = window;
				mEntry = entry;
			}

			public void Dispose ()
			{
				mWindow.Entries.Remove(mEntry);
				mEntry.Selected -= mWindow.OnEntrySelected;
			}
		}
	}
}
