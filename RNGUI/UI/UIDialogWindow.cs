﻿//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System;

	/// <summary>
	/// The window with multiply dialogue actions.
	/// </summary>
	public class UIDialogWindow : UIWindow
	{
		/// <summary>
		/// This event is raised when dialog entry selected.
		/// </summary>
		public event WindowOptionEventHandler<UIDialogWindow, UIDialogOption> DialogSelected;

		/// <summary>
		/// The dialog window layout settings.
		/// </summary>
		public Layout layout = new Layout();

		/// <summary>
		/// The message to sent when confirmed.
		/// </summary>
		public Message onConfirm = new Message();

		/// <summary>
		/// The message to sent when canceled.
		/// </summary>
		public Message onCancel = new Message();

		/// <summary>
		/// The message to sent when rejected.
		/// </summary>
		public Message onReject = new Message();

		/// <summary>
		/// Create new instance of the <see cref="UIDialogWindow"/>.
		/// </summary>
		protected UIDialogWindow ()
		{
			Selected += OnSelected;
		}

		/// <summary>
		/// The text of the header.
		/// </summary>
		public string HeaderText
		{
			get { return layout != null && layout.header != null ? layout.header.text : null; }
			set { if (layout != null && layout.header != null) layout.header.text = value; }
		}

		/// <summary>
		/// The text of the message.
		/// </summary>
		public string MessageText
		{
			get { return layout != null && layout.message != null ? layout.message.text : null; }
			set { if (layout != null && layout.message != null) layout.message.text = value; }
		}

		/// <summary>
		/// The text of the confirm button.
		/// </summary>
		public string ConfirmText
		{
			get { return layout != null && layout.confirmText != null ? layout.confirmText.text : null; }
			set { if (layout != null && layout.confirmText != null) layout.confirmText.text = value; }
		}

		/// <summary>
		/// The text of the reject button.
		/// </summary>
		public string RejectText
		{
			get { return layout != null && layout.rejectText != null ? layout.rejectText.text : null; }
			set { if (layout != null && layout.rejectText != null) layout.rejectText.text = value; }
		}

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public override void Awake ()
		{
			onConfirm.SetDefaultReceiver(gameObject);
			onReject.SetDefaultReceiver(gameObject);
			onCancel.SetDefaultReceiver(gameObject);

			base.Awake();
		}

		/// <summary>
		/// Get the window entry with specified dialog option.
		/// </summary>
		/// <param name="option">The dialog option of the entry.</param>
		public UIWindowEntry GetEntry (UIDialogOption option)
		{
			return GetEntry((int)option);
		}

		/// <summary>
		/// The callback for dialog confirm.
		/// </summary>
		protected virtual void OnConfirm ()
		{
			CloseWindow();
		}

		/// <summary>
		/// The callback for dialog cancel.
		/// </summary>
		protected virtual void OnCancel ()
		{
			CloseWindow();
		}

		/// <summary>
		/// he callback for dialog reject.
		/// </summary>
		protected virtual void OnReject ()
		{
			CloseWindow();
		}

		/// <summary>
		/// The callback for dialog entry selection.
		/// </summary>
		private void OnSelected (UIWindow window, int entryID)
		{
			if (!Enum.IsDefined(typeof(UIDialogOption), entryID))
				return;

			switch (entryID)
			{
				case (int)UIDialogOption.Confirm:
					OnConfirm();
					if (onConfirm != null) onConfirm.Send();
					break;

				case (int)UIDialogOption.Cancel:
					OnCancel();
					if (onCancel != null) onCancel.Send();
					break;

				case (int)UIDialogOption.Reject:
					OnReject();
					if (onReject != null) onReject.Send();
					break;
			}

			OnDialogSelected((UIDialogOption)entryID);
		}

		/// <summary>
		/// Raise <see cref="DialogSelected"/> event.
		/// </summary>
		private void OnDialogSelected (UIDialogOption entry)
		{
			if (DialogSelected != null)
				DialogSelected(this, entry);
		}

		[Serializable]
		public class Layout
		{
			/// <summary>
			/// The window header text.
			/// </summary>
			public UILabel header;

			/// <summary>
			/// The window message text.
			/// </summary>
			public UILabel message;

			/// <summary>
			/// The confirm button displayed text.
			/// </summary>
			public UILabel confirmText;

			/// <summary>
			/// The reject button displayed text.
			/// </summary>
			public UILabel rejectText;
		}
	}
}
