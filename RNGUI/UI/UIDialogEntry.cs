//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	/// <summary>
	/// The entry of the <see cref="UIDialogWindow"/>.
	/// </summary>
	public class UIDialogEntry : UIWindowEntry
	{
		/// <summary>
		/// The dialog window ID.
		/// </summary>
		public UIDialogOption option = UIDialogOption.None;

		/// <summary>
		/// The ID of the window entry operation.
		/// </summary>
		public override int ID { get { return (int)option; } }
	}
}
