//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System;
	using UnityEngine;

	/// <summary>
	/// Virtual analog joystick.
	/// </summary>
	[AddComponentMenu("RNGUI/UI/Joystick")]
	public class UIJoystick : MonoBehaviour
	{
		private Vector2 mFirstTouch;
		private Vector3 mLastPosition;

		/// <summary>
		/// The parent thumb holder transform.
		/// </summary>
		public Transform root;

		/// <summary>
		/// The floating transform under the thumb.
		/// </summary>
		public Transform thumb;

		/// <summary>
		/// The radius of the thumb working area.
		/// </summary>
		public float radius;

		/// <summary>
		/// Radius of the joystick insensitivity zone.
		/// </summary>
		public float deadZone;

		/// <summary>
		/// Whether to place joystick under thumb.
		/// </summary>
		public bool underTouch;

		/// <summary>
		/// Whether to return joystick to initial position when not active.
		/// </summary>
		public bool returnJoystick;

		/// <summary>
		/// Axis X settings.
		/// </summary>
		public Axis axisX;

		/// <summary>
		/// Axis Y settings.
		/// </summary>
		public Axis axisY;

		/// <summary>
		/// The amount of digits after integer in normalized value.
		/// </summary>
		public int digits = 2;

		/// <summary>
		/// The current offset of the joystick.
		/// </summary>
		public Vector2 offset;

		/// <summary>
		/// The normalized offset of the joystick.
		/// </summary>
		public Vector2 normalize;

		/// <summary>
		/// Whether joystick is currently pressed.
		/// </summary>
		public bool isPressed;

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		public void OnEnable ()
		{
			if (root != null)
				mLastPosition = root.localPosition;
		}

		/// <summary>
		/// Change the center position of the joystick.
		/// </summary>
		/// <param name="center">Position of the new center.</param>
		public void SetCenter (Vector2 center)
		{
			if (root != null) root.position = center;
			if (thumb != null) thumb.position = center;
		}

		/// <summary>
		/// Clamp joystick axis.
		/// </summary>
		/// <param name="axis">The XY axis values.</param>
		/// <returns>Returns axis plane.</returns>
		public Vector2 ClampAxis (Vector2 axis)
		{
			axis = Vector2.ClampMagnitude(axis, radius);
			axis = new Vector2(axisX.Clamp(axis.x), axisY.Clamp(axis.y));
			return axis.magnitude < deadZone ? Vector2.zero : axis;
		}

		/// <summary>
		/// Refresh joystick values.
		/// </summary>
		public void RefreshJoystick ()
		{
			// Normalize direction and gravity
			normalize = offset / radius;

			// Round to digits
			normalize.x = Convert.ToSingle(Math.Round(normalize.x, digits));
			normalize.y = Convert.ToSingle(Math.Round(normalize.y, digits));

			// Update thumb position
			if (thumb != null)
			{
				if (underTouch)
					thumb.position = UICamera.currentCamera.ScreenToWorldPoint(mFirstTouch + offset);
				else
					thumb.localPosition = offset;
			}
		}

		/// <summary>
		/// Reset joystick values.
		/// </summary>
		public void ResetJoystick ()
		{
			mFirstTouch = Vector2.zero;
			offset = Vector2.zero;
			normalize = Vector2.zero;
			if (thumb != null) thumb.localPosition = Vector2.zero;
			if (returnJoystick && root != null) root.localPosition = mLastPosition;
		}

		/// <summary>
		/// Received when a mouse button gets pressed on the collider.
		/// </summary>
		public void OnPress (bool pIsPressed)
		{
			isPressed = pIsPressed;

			if (pIsPressed)
			{
				if (underTouch)
				{
					mFirstTouch = UICamera.lastTouchPosition;
					SetCenter(UICamera.lastHit.point);
				}
				else
				{
					mFirstTouch = UICamera.currentCamera.WorldToScreenPoint((thumb ?? transform).position);

					// Set joystick on initial position
					OnDrag(mFirstTouch);
				}
			}
			else
			{
				// Reset joystick
				ResetJoystick();
			}
		}

		/// <summary>
		/// Received when a mouse or touch gets pressed on a collider and starts dragging it.
		/// </summary>
		public void OnDrag (Vector2 delta)
		{
			// Calculate delta
			var mDelta = underTouch
						? UICamera.currentTouch.totalDelta
						: UICamera.currentTouch.pos - mFirstTouch;

			// Calculate offset
			offset = ClampAxis(mDelta);

			// Round to integer
			offset.x = Mathf.Round(offset.x);
			offset.y = Mathf.Round(offset.y);

			// Refresh joystick
			RefreshJoystick();
		}

		/// <summary>
		/// Joystick axis settings.
		/// </summary>
		[Serializable]
		public class Axis
		{
			/// <summary>
			/// Whether to invert axis value.
			/// </summary>
			public bool invert;

			/// <summary>
			/// Axis value threshold.
			/// </summary>
			public float threshold;

			/// <summary>
			/// Scale factor of the axis.
			/// </summary>
			public float scale = 1;

			/// <summary>
			/// Clamp axis value.
			/// </summary>
			/// <param name="value">The value of the axis.</param>
			public float Clamp (float value)
			{
				// Apply scale
				value *= scale;

				// Apply inversion
				if (invert) value *= -1;

				// Check threshold
				return Mathf.Abs(value) < threshold ? 0 : value;
			}
		}
	}
}