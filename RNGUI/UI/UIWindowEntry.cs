﻿//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;
	using System;

	/// <summary>
	/// The base class for entries in <see cref="UIWindow"/>.
	/// </summary>
	public class UIWindowEntry : MonoBehaviour, IDisposable
	{
		private IDisposable _eventHandler;

		/// <summary>
		/// The instance of the parent window.
		/// </summary>
		public UIWindow window;

		/// <summary>
		/// The UI event trigger.
		/// </summary>
		public UIEventType eventType = UIEventType.OnClick;

		/// <summary>
		/// The window action to execute on entry selection.
		/// </summary>
		public WindowAction windowAction = WindowAction.None;

		/// <summary>
		/// The message to send on entry selection.
		/// </summary>
		public Message onSelected = new Message();

		/// <summary>
		/// This event is raised when current entry selected.
		/// </summary>
		public event Event<UIWindowEntry> Selected;

		/// <summary>
		/// The ID of the window entry operation.
		/// </summary>
		public virtual int ID { get; private set; }

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public virtual void Awake ()
		{
			if (window == null) window = FindWindow();
			onSelected.SetDefaultReceiver(gameObject);
			UIEventTrace.Get(gameObject).EventTraced += OnEventTraced;
		}

		/// <summary>
		/// Start is called just before any of the Update methods is called the first time.
		/// </summary>
		public virtual void Start ()
		{

		}

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		public virtual void OnEnable ()
		{
			// Dispose previous events.
			Dispose();

			if (window != null)
				_eventHandler = window.Attach(this);
		}

		/// <summary>
		/// This function is called when the behaviour becomes disabled () or inactive.
		/// </summary>
		public virtual void OnDisable ()
		{

		}

		/// <summary>
		/// Dispose event subscription.
		/// </summary>
		public void Dispose ()
		{
			if (_eventHandler == null) return;
			_eventHandler.Dispose();
			_eventHandler = null;
		}

		/// <summary>
		/// Find root window for this entry.
		/// </summary>
		protected UIWindow FindWindow ()
		{
			return NGUITools.FindInParents<UIWindow>(gameObject);
		}

		/// <summary>
		/// Raise <see cref="Selected"/> event.
		/// </summary>
		private void OnSelected ()
		{
			if (Selected != null) Selected(this);
			if (onSelected != null) onSelected.Send();
			if (window != null) window.ExecuteAction(windowAction);
		}

		/// <summary>
		/// UI event trace callback
		/// </summary>
		private void OnEventTraced (UIEventTrace sender, UIEventType pEventType)
		{
			if (eventType == pEventType)
				OnSelected();
		}
	}
}
