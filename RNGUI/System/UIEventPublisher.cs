﻿//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System;
	using UnityEngine;
	using System.Collections.Generic;

	/// <summary>
	/// This script allows to publish received event to the attached gameobject via SendMessage.
	/// </summary>
	[AddComponentMenu("RNGUI/System/Publish Events")]
	public class UIEventPublisher : MonoBehaviour
	{
		/// <summary>
		/// Collection of receivers, to which events will be published.
		/// </summary>
		public List<GameObject> receivers;

		/// <summary>
		/// Collection of events to suppress publishing.
		/// </summary>
		public List<UIEventType> suppress;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			UIEventTrace.Get(gameObject).EventTraced += OnTraced;
		}

		/// <summary>
		/// Callback for received UI events.
		/// </summary>
		private void OnTraced (UIEventTrace sender, UIEventType eventType)
		{
			// Publish only allowed events
			if (suppress.Contains(eventType)) return;

			// The list of events to suppress publishing
			switch (eventType)
			{
				case UIEventType.None:
				case UIEventType.OnStart:
				case UIEventType.OnEnable:
				case UIEventType.OnDisable:
				case UIEventType.OnTriggerEnter:
					return;
			}

			var mHasCircularReference = false;
			var mFunction = eventType.GetFunctionName();
			var mObj = GetParameter(sender, eventType);

			foreach (var mGameObject in receivers)
			{
				if (mGameObject == gameObject)
				{
					mHasCircularReference = true;
					Debug.LogWarning("Cant publish UI Events on self - this will lead to circular reference. GameObject: " + NGUITools.GetHierarchy(gameObject));
					continue;
				}

				if (mObj == null)
					mGameObject.SendMessage(mFunction, SendMessageOptions.DontRequireReceiver);
				else
					mGameObject.SendMessage(mFunction, mObj, SendMessageOptions.DontRequireReceiver);
			}

			// Cleanup circular references.
			if (mHasCircularReference) receivers.RemoveAll(x => x == gameObject);
		}

		/// <summary>
		/// Resolve which argument to send along with the message.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventType">The type of the event.</param>
		private static object GetParameter (UIEventTrace sender, UIEventType eventType)
		{
			switch (eventType)
			{
				// Events without parameters
				case UIEventType.None:
				case UIEventType.OnClick:
				case UIEventType.OnDoubleClick:
				case UIEventType.OnDisable:
				case UIEventType.OnEnable:
				case UIEventType.OnStart:
				case UIEventType.OnTriggerEnter:
					return null;

				// Events with always TRUE parameter
				case UIEventType.OnHoverTrue:
				case UIEventType.OnPressTrue:
				case UIEventType.OnSelectTrue:
				case UIEventType.OnActivateTrue:
					return true;

				// Events with always FALSE parameter
				case UIEventType.OnHoverFalse:
				case UIEventType.OnPressFalse:
				case UIEventType.OnSelectFalse:
				case UIEventType.OnActivateFalse:
					return false;

				// Events with parameter depending on current sender state
				case UIEventType.OnHover: return sender.IsOver;
				case UIEventType.OnPress: return sender.IsPressed;
				case UIEventType.OnSelect: return sender.IsSelected;
				case UIEventType.OnActivate: return sender.IsActive;
				case UIEventType.OnDrag: return sender.Delta;

				default:
					throw new ArgumentOutOfRangeException("eventType", eventType, string.Format("Event Type {0} is not supported.", eventType));
			}
		}
	}
}

