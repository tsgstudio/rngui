//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System.Collections.Generic;
	using AnimationOrTween;
	using UnityEngine;

	/// <summary>
	/// The events trace class lets you trace all NGUI events.
	/// </summary>
	[AddComponentMenu("RNGUI/System/Trace Events")]
	public class UIEventTrace : MonoBehaviour
	{
		#region Cache
		private static readonly Dictionary<int, UIEventTrace> BaseDictionary = new Dictionary<int, UIEventTrace>();

		/// <summary>
		/// Commit changes to the cache.
		/// </summary>
		/// <param name="id">The key of the <see cref="UIEventTrace"/> instance.</param>
		/// <param name="value">The instance of the <see cref="UIEventTrace"/> to cache.</param>
		/// <param name="state">The state of the object: True -> add to chache; False -> remove from chache.</param>
		private static void Cache (int id, UIEventTrace value, bool state)
		{
			// Already cached
			if (value.mCached == state) return;

			if (state) // Add to cache
			{
				BaseDictionary[id] = value;
			}
			else // Remove from cache
			{
				BaseDictionary.Remove(id);
			}

			// Update state of the object
			value.mCached = state;
		}
		#endregion

		/// <summary>
		/// Represents the method that will handle an event.
		/// </summary>
		/// <param name="sender">Source of the event.</param>
		/// <param name="eventType">Received UI event.</param>
		public delegate void EventHandler (UIEventTrace sender, UIEventType eventType);

		/// <summary>
		/// Get or add an <see cref="UIEventTrace"/> to the specified game object.
		/// </summary>
		public static UIEventTrace Get (GameObject go)
		{
			UIEventTrace mRetVal;
			BaseDictionary.TryGetValue(go.GetInstanceID(), out mRetVal);
			return mRetVal ?? go.AddComponent<UIEventTrace>();
		}

		/// <summary>
		/// Get or add an <see cref="UIEventTrace"/> to the specified game object.
		/// </summary>
		public static UIEventTrace Get (GameObject go, EventHandler eventHandler)
		{
			var mRetVal = Get(go);
			mRetVal.EventTraced += eventHandler;
			return mRetVal;
		}

		/// <summary>
		/// Translate event to specified gameobject.
		/// </summary>
		/// <param name="go">The target gameobject to which, event will be translated.</param>
		/// <param name="eventType">The type of the event to translate.</param>
		public static void Translate (GameObject go, UIEventType eventType)
		{
			Get(go).Notify(eventType);
		}

		/// <summary>
		/// Raised when new <see cref="Trigger"/> received.
		/// </summary>
		public event EventHandler EventTraced;

		private int mGameobjectID;
		private bool mCached = false;
		private bool mStarted = false;
		private bool mHighlighted = false;

		/// <summary>
		/// Whether mouse currently hovers over a collider.
		/// </summary>
		public bool IsOver { get; private set; }

		/// <summary>
		/// Whether mouse button is currently pressed on the collider.
		/// </summary>
		public bool IsPressed { get; private set; }

		/// <summary>
		/// Whether mouse is released on the same object as it was pressed on.
		/// </summary>
		public bool IsSelected { get; private set; }

		/// <summary>
		/// Whether <see cref="UICheckbox"/> is checked.
		/// </summary>
		public bool IsActive { get; private set; }

		/// <summary>
		/// The amount of drag delta.
		/// </summary>
		public Vector2 Delta { get; private set; }

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			mGameobjectID = gameObject.GetInstanceID();
			Cache(mGameobjectID, this, true);
		}

		/// <summary>
		/// Start is called just before any of the Update methods is called the first time.
		/// </summary>
		public void Start ()
		{
			mStarted = true;
			Notify(UIEventType.OnStart);
		}

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		public void OnEnable ()
		{
			Notify(UIEventType.OnEnable);

			if (mStarted && mHighlighted)
				OnHover(UICamera.IsHighlighted(gameObject));
		}

		/// <summary>
		/// This function is called when the behaviour becomes disabled () or inactive.
		/// </summary>
		public void OnDisable ()
		{
			Notify(UIEventType.OnDisable);
		}

		/// <summary>
		/// This function is called when the MonoBehaviour will be destroyed.
		/// </summary>
		public void OnDestroy ()
		{
			Cache(mGameobjectID, this, false);
		}

		/// <summary>
		/// Is sent when the mouse hovers over a collider or moves away.
		/// </summary>
		public void OnHover (bool isOver)
		{
			if (!enabled) return;
			IsOver = isOver;
			Notify(UIEventType.OnHover);
			Notify(IsOver ? UIEventType.OnHoverTrue : UIEventType.OnHoverFalse);
			mHighlighted = isOver;
		}

		/// <summary>
		/// Is sent when a mouse button gets pressed on the collider.
		/// </summary>
		public void OnPress (bool isPressed)
		{
			if (!enabled) return;
			IsPressed = isPressed;
			Notify(UIEventType.OnPress);
			Notify(IsPressed ? UIEventType.OnPressTrue : UIEventType.OnPressFalse);
		}

		/// <summary>
		/// Is sent when a mouse button is released on the same object as it was pressed on.
		/// </summary>
		public void OnSelect (bool isSelected)
		{
			if (!enabled) return;
			IsSelected = isSelected;
			Notify(UIEventType.OnSelect);
			Notify(IsSelected ? UIEventType.OnSelectTrue : UIEventType.OnSelectFalse);
		}

		/// <summary>
		/// Is sent with the same conditions as OnSelect, with the added check to see if the mouse has not moved much.
		/// </summary>
		public void OnClick ()
		{
			if (!enabled) return;
			Notify(UIEventType.OnClick);
		}

		/// <summary>
		/// Is sent when the click happens twice within a fourth of a second.
		/// </summary>
		public void OnDoubleClick ()
		{
			if (!enabled) return;
			Notify(UIEventType.OnDoubleClick);
		}

		/// <summary>
		/// Is sent when the <see cref="UICheckbox"/> state changes.
		/// </summary>
		/// <param name="isActive"></param>
		public void OnActivate (bool isActive)
		{
			if (!enabled) return;
			IsActive = isActive;
			Notify(UIEventType.OnActivate);
			Notify(IsActive ? UIEventType.OnActivateTrue : UIEventType.OnActivateFalse);
		}

		/// <summary>
		/// Is sent when a mouse or touch gets pressed on a collider and starts dragging it.
		/// </summary>
		/// <param name="delta"></param>
		public void OnDrag (Vector2 delta)
		{
			Delta = delta;
			Notify(UIEventType.OnDrag);
		}

		/// <summary>
		/// This is called when the Collider other enters the trigger.
		/// </summary>
		/// <param name="other"></param>
		public void OnTriggerEnter (Collider other)
		{
			if (!enabled) return;
			Notify(UIEventType.OnTriggerEnter);
		}

		/// <summary>
		/// Notify subscribers about traced event.
		/// </summary>
		private void Notify (UIEventType eventType)
		{
			if (EventTraced != null)
				EventTraced(this, eventType);
		}
	}
}
