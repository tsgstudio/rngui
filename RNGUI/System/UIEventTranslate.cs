﻿//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System.Collections.Generic;
	using UnityEngine;

	/// <summary>
	/// The script, which translates UI events from current object to remote objects.
	/// </summary>
	[AddComponentMenu("RNGUI/System/Translate Events")]
	public class UIEventTranslate : MonoBehaviour
	{
		/// <summary>
		/// Collection of receivers, to which events will be transltaed.
		/// </summary>
		public List<GameObject> receivers;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			UIEventTrace.Get(gameObject).EventTraced += OnTraced;
		}

		/// <summary>
		/// Callback for received UI evets.
		/// </summary>
		private void OnTraced (UIEventTrace sender, UIEventType eventType)
		{
			var mIsDirty = false;
			foreach (var mRemote in receivers)
			{
				// Skip destroyed objects
				if (mRemote == null) { mIsDirty = true; continue; }

				// Translate event
				UIEventTrace.Translate(mRemote, eventType);
			}

			// Remove destroyed objects
			if (mIsDirty) receivers.RemoveAll(x => x == null);
		}
	}
}
