//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	/// <summary>
	/// Represent the ID of the dialog event.
	/// </summary>
	public enum UIDialogOption
	{
		/// <summary>
		/// Dialog window have been rejected.
		/// </summary>
		Reject = -2,

		/// <summary>
		/// Dialog window have been canceled.
		/// </summary>
		Cancel = -1,

		/// <summary>
		/// Dialog window have not been changed.
		/// </summary>
		None = 0,

		/// <summary>
		/// Dialog window have been confirmed.
		/// </summary>
		Confirm = 1,

		/// <summary>
		/// Dialog window alt choice have been selected.
		/// </summary>
		Alt = 2,

		/// <summary>
		/// Dialog window second alt choice have been selected.
		/// </summary>
		Alt2 = 3,

		/// <summary>
		/// Dialog window other choice have been selected.
		/// </summary>
		Other = int.MaxValue
	}
}

