//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	/// <summary>
	/// Represents the method that will handle an event.
	/// </summary>
	/// <typeparam name="TSource">Type of the source of the event.</typeparam>
	/// <param name="sender">The source of the event.</param>
	public delegate void Event<TSource> (TSource sender);

	/// <summary>
	/// Represents the method that will handle an event.
	/// </summary>
	/// <typeparam name="TSource">The type of the source of the event.</typeparam>
	/// <typeparam name="TEventArgs">The type of the event data generated by the event.</typeparam>
	/// <param name="sender">The source of the event.</param>
	/// <param name="e">Contains the event data/</param>
	public delegate void Event<TSource, TEventArgs> (TSource sender, TEventArgs e);
}
