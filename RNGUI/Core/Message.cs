//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System;
	using UnityEngine;

	/// <summary>
	/// The Unity3D message.
	/// </summary>
	[Serializable]
	public class Message
	{
		/// <summary>
		/// Whether to suppress message sending.
		/// </summary>
		public bool enabled = false;

		/// <summary>
		/// The gameobject, which will receive message.
		/// </summary>
		public GameObject receiver;

		/// <summary>
		/// Whether to make sure receiver is enabled.
		/// </summary>
		public bool enableReceiver = false;

		/// <summary>
		/// The name of the function in the message.
		/// </summary>
		public string function;

		/// <summary>
		/// Options for how to send a message.
		/// </summary>
		public SendMessageOptions options = SendMessageOptions.DontRequireReceiver;

		/// <summary>
		/// Whether the child game objects will be included.
		/// </summary>
		public bool children = false;

		/// <summary>
		/// Send the message to gameobject targete by <see cref="receiver"/>.
		/// </summary>
		public void Send ()
		{
			Send(receiver);
		}

		/// <summary>
		/// Send the message to the specified receiver.
		/// </summary>
		/// <param name="pReceiver">The gameobject which will receive the message.</param>
		public void Send (GameObject pReceiver)
		{
			if (pReceiver == null || !enabled) return;

			// Enable receiver
			if (enableReceiver && !NGUITools.GetActive(pReceiver))
				NGUITools.SetActive(pReceiver, true);

			// Do not send empty message
			if (string.IsNullOrEmpty(function)) return;

			// Send message
			if (children) pReceiver.BroadcastMessage(function, SendMessageOptions.DontRequireReceiver);
			else pReceiver.SendMessage(function, SendMessageOptions.DontRequireReceiver);
		}
	}
}
