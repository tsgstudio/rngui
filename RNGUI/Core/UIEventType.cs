﻿//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System;

	/// <summary>
	/// Represent the type of the UI event.
	/// </summary>
	[Flags]
	public enum UIEventType
	{
		None = 0,
		OnClick,
		OnDoubleClick,
		OnHover,
		OnPress,
		OnSelect,
		OnActivate,
		OnDrag,

		OnHoverTrue = 100,
		OnHoverFalse,
		OnPressTrue,
		OnPressFalse,
		OnSelectTrue,
		OnSelectFalse,
		OnActivateTrue,
		OnActivateFalse,

		OnEnable = 200,
		OnDisable,
		OnStart,

		OnTriggerEnter = 300,
	}
}
