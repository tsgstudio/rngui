//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System;
	using UnityEngine;

	/// <summary>
	/// Script which reacts on UIPanel events.
	/// </summary>
	[AddComponentMenu("RNGUI/Control/Reactive Panel")]
	public class UIPanelReactive : MonoBehaviour
	{
		private UIDraggablePanel mPanel;
		private UIScrollBar mVerticalScroll;
		private UIScrollBar mHorizontalScroll;
		private SpringPanel mSpringPanel;
		private bool mSpringPanelSubscribed;

		/// <summary>
		/// Raised when panel have finished drag operation.
		/// </summary>
		public event Action DragFinished;

		/// <summary>
		/// Raised when pane have finished spring operation.
		/// </summary>
		public event Action SpringFinished;

		/// <summary>
		/// This message will be sent when drag operation finished.
		/// </summary>
		public Message onDragFinished = new Message { function = "OnDragFinished" };

		/// <summary>
		/// This message will be sent when spring operation finished.
		/// </summary>
		public Message onSpringFinished = new Message { function = "OnSpringFinished" };

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			// Cached gameobject
			var mGameObject = gameObject;

			onDragFinished.SetDefaultReceiver(mGameObject);
			onSpringFinished.SetDefaultReceiver(mGameObject);

			if (mPanel == null)
				mPanel = NGUITools.FindInParents<UIDraggablePanel>(mGameObject);

			if (mPanel == null)
			{
				Debug.LogWarning("UIPanel is not found on: " + NGUITools.GetHierarchy(mGameObject));
				return;
			}

			// Subscrie panel drag events
			mPanel.onDragFinished += OnDragFinishedInternal;

			// Check for attached scrollbars
			mVerticalScroll = mPanel.verticalScrollBar;
			mHorizontalScroll = mPanel.horizontalScrollBar;

			// Subscribe scroll bar drag events
			if (mVerticalScroll != null) mVerticalScroll.onDragFinished += OnDragFinishedInternal;
			if (mHorizontalScroll != null) mHorizontalScroll.onDragFinished += OnDragFinishedInternal;

			// Check for spring panel
			CheckSpringPanel();
		}

		/// <summary>
		/// Update is called every frame, if the MonoBehaviour is enabled.
		/// </summary>
		public void Update ()
		{
			if (mSpringPanel == null) return;

			if (mSpringPanel.enabled)
			{
				// Resubscribe if event deleted
				mSpringPanelSubscribed &= mSpringPanel.onFinished != null;

				if (!mSpringPanelSubscribed)
				{
					// Resubscribe
					mSpringPanel.onFinished += OnSpringFinishedInternal;
					mSpringPanelSubscribed = true;
				}
			}
			else
			{
				mSpringPanelSubscribed = false;
			}
		}

		/// <summary>
		/// Protected callback for finished drag operation.
		/// </summary>
		protected virtual void OnDragFinished ()
		{

		}

		/// <summary>
		/// Protected callback for finished spring operation.
		/// </summary>
		protected virtual void OnSpringFinished ()
		{

		}

		/// <summary>
		/// Check whether spring panel attached to dragable panel.
		/// </summary>
		private void CheckSpringPanel ()
		{
			if (mPanel == null || mPanel.dragEffect == UIDraggablePanel.DragEffect.None) return;

			if (mSpringPanel == null)
				mSpringPanel = mPanel.GetComponent<SpringPanel>() ?? mPanel.gameObject.GetComponent<SpringPanel>();
		}

		/// <summary>
		/// Private callback for finished drag operation.
		/// </summary>
		private void OnDragFinishedInternal ()
		{
			CheckSpringPanel();

			// Raise callback
			OnDragFinished();

			// Raise event
			if (DragFinished != null) DragFinished();

			// Send messages
			if (onDragFinished != null) onDragFinished.Send();
		}

		/// <summary>
		/// Private callback for finished drag operation.
		/// </summary>
		private void OnSpringFinishedInternal ()
		{
			// Raise callback
			OnSpringFinished();

			// Raise event
			if (SpringFinished != null) SpringFinished();

			// Send messages
			if (onSpringFinished != null) onSpringFinished.Send();
		}
	}
}
