//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// The component for <see cref="UISlider.fullSize"/> setup.
	/// </summary>
	[ExecuteInEditMode]
	[RequireComponent(typeof(UISlider))]
	public class UISliderSize : MonoBehaviour
	{
		private bool _hasFirstChange;
		[SerializeField]
		private bool _resizeNow;
		private UISlider _slider;

		/// <summary>
		/// The instance of the slider background.
		/// </summary>
		public Transform background;

		/// <summary>
		/// The scale offset from background scale.
		/// </summary>
		public Vector2 offset = new Vector2(2, 2);

		/// <summary>
		/// Resize the slider when first change received.
		/// </summary>
		public bool updateOnFirstChange = true;

		/// <summary>
		/// The UI event to trigger size update.
		/// </summary>
		public UIEventType eventType = UIEventType.OnStart;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			_slider = GetComponent<UISlider>();
			if (_slider != null)
				_slider.onValueChange += OnValueChange;

			if (Application.isPlaying)
				UIEventTrace.Get(gameObject).EventTraced += OnEventTraced;
		}

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		public void OnEnable ()
		{
#if UNITY_EDITOR
			if (_slider == null)
				_slider = GetComponent<UISlider>();
#endif
		}

		/// <summary>
		/// Update is called every frame, if the MonoBehaviour is enabled.
		/// </summary>
		public void Update ()
		{
			if (_resizeNow)
			{
				_resizeNow = false;
				UpdateSize();
			}
		}

		/// <summary>
		/// Update the slider size.
		/// </summary>
		public void UpdateSize ()
		{
			if (background == null) return;
			var mLocalScale = background.localScale;
			var mRetVal = new Vector2(mLocalScale.x - offset.x, mLocalScale.y - offset.y);
			_slider.fullSize = mRetVal;
		}

		/// <summary>
		/// Callback for UI events.
		/// </summary>
		private void OnEventTraced (UIEventTrace sender, UIEventType pEventType)
		{
			if (pEventType == eventType)
				UpdateSize();
		}

		/// <summary>
		/// Callback for slider changes.
		/// </summary>
		private void OnValueChange (float val)
		{
			if (_hasFirstChange || !updateOnFirstChange) return;

			_hasFirstChange = true;
			UpdateSize();
		}
	}
}

