//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// Activate, deactivate or change state of the game object via SendMessage.
	/// </summary>
	[AddComponentMenu("RNGUI/Control/Controlled Object")]
	public class UIControlledObject : MonoBehaviour
	{
		protected UICheckbox mCheckbox;

		/// <summary>
		/// Target object which state will be controlled.
		/// </summary>
		public GameObject target;

		/// <summary>
		/// The initial state of the <see cref="target"/> gameobject.
		/// </summary>
		public bool initialState = true;

		/// <summary>
		/// Whether to inverse activation calls.
		/// This is valid only for <see cref="OnActivate"/> calls.
		/// </summary>
		public bool inverse = false;

		/// <summary>
		/// Whether to copy state of the current object to the target object.
		/// </summary>
		public bool echo = false;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			if (target == null) target = gameObject;
			mCheckbox = GetComponent<UICheckbox>();
			NGUITools.SetActive(target, initialState);
		}

		/// <summary>
		/// Start is called just before any of the Update methods is called the first time.
		/// </summary>
		public void Start ()
		{

		}

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		public void OnEnable ()
		{
			// Compatibility with UICheckbox
			if (echo) OnActivate(true);
			else if (mCheckbox != null) OnActivate(mCheckbox.isChecked);
		}

		/// <summary>
		/// This function is called when the behaviour becomes disabled () or inactive.
		/// </summary>
		public void OnDisable ()
		{
			if (echo) OnActivate(false);
		}

		/// <summary>
		/// Activate <see cref="target"/> gameobject.
		/// </summary>
		public void Activate () { SetState(true); }

		/// <summary>
		/// Deactivate <see cref="target"/> gameobject.
		/// </summary>
		public void Deactivate () { SetState(false); }

		/// <summary>
		/// Change state of the <see cref="target"/> to opposite.
		/// </summary>
		public void Reverse () { SetState(!NGUITools.GetActive(target)); }

		/// <summary>
		/// Callback for <see cref="UICheckbox"/> compatibility.
		/// </summary>
		public void OnActivate (bool isActive) { SetState(inverse ? !isActive : isActive); }

		/// <summary>
		/// Change state of the <see cref="target"/> gameobject.
		/// </summary>
		public void SetState (bool state)
		{
			if (target == null) return;
			NGUITools.SetActive(target, state);
			var mPanel = NGUITools.FindInParents<UIPanel>(target);
			if (mPanel != null) mPanel.Refresh();
		}
	}
}
