﻿//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// This script will send message when matching UI event received.
	/// </summary>
	[AddComponentMenu("RNGUI/Control/Messanger")]
	public class UIMessenger : IgnoreTimeScale
	{
		private float _nextRepeatTime;

		/// <summary>
		/// The current time denpending on <see cref="ignoreTimeScale"/> option value.
		/// </summary>
		protected float TimeNow { get { return ignoreTimeScale ? realTime : Time.time; } }

		/// <summary>
		/// The delay in milisecconds before sending message.
		/// </summary>
		public int msDelay = 0;

		/// <summary>
		/// Whether to ignore <see cref="Time.timeScale"/>.
		/// </summary>
		public bool ignoreTimeScale = true;

		/// <summary>
		/// Whether to continue sending messages after delay.
		/// </summary>
		public bool repeat = false;

		/// <summary>
		/// The type of the UI event, which will trigger to send message.
		/// </summary>
		public UIEventType eventType;

		/// <summary>
		/// The message to send.
		/// </summary>
		public Message message = new Message();

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			UpdateRealTimeDelta();
			message.SetDefaultReceiver(gameObject);
			UIEventTrace.Get(gameObject).EventTraced += OnTraced;
		}

		/// <summary>
		/// This function is called just before any of the Update methods is called the first time.
		/// </summary>
		public void Start ()
		{
			if (eventType == UIEventType.OnStart)
				SendAndScheduleNext();
		}

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		protected override void OnEnable ()
		{
			base.OnEnable();

			if (eventType == UIEventType.OnEnable)
				SendAndScheduleNext();
		}

		/// <summary>
		/// This function is called when the behaviour becomes disabled () or inactive.
		/// </summary>
		public void OnDisable ()
		{
			if (eventType == UIEventType.OnDisable)
				SendAndScheduleNext();
		}

		/// <summary>
		/// Update is called every frame, if the MonoBehaviour is enabled.
		/// </summary>
		public void Update ()
		{
			UpdateRealTimeDelta();

			if (_nextRepeatTime <= 0) return;
			if (_nextRepeatTime > TimeNow) return;

			SendAndScheduleNext();
		}

		/// <summary>
		/// Send message and schedule next message send time.
		/// </summary>
		public void SendAndScheduleNext ()
		{
			ScheduleNext();
			message.Send();
		}

		/// <summary>
		/// Schedule next message send time.
		/// </summary>
		private void ScheduleNext ()
		{
			_nextRepeatTime = repeat ? TimeNow + (msDelay / 1000f) : -1f;
		}

		/// <summary>
		/// Callback for UI events.
		/// </summary>
		private void OnTraced (UIEventTrace sender, UIEventType uiEvent)
		{
			if (eventType == UIEventType.None) return;
			if (eventType != uiEvent) return;
			SendAndScheduleNext();
		}
	}
}
