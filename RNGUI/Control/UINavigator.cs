//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System;
	using System.Collections.Generic;
	using UnityEngine;

	/// <summary>
	/// This script allows easily navigate through different menu.
	/// </summary>
	public class UINavigator : MonoBehaviour
	{
		/// <summary>
		/// The history of the navigations.
		/// </summary>
		private static readonly Stack<GameObject> History = new Stack<GameObject>();

		/// <summary>
		/// Get the last navigation point.
		/// </summary>
		/// <returns></returns>
		public static GameObject GetLastPoint ()
		{
			do
			{
				if (History.Count <= 0) return null;
			} while (History.Peek() == null);

			return History.Pop();
		}

		/// <summary>
		/// Navigate from one point to another without return path.
		/// </summary>
		/// <param name="from">The outgoing point.</param>
		/// <param name="to">The incoming point.</param>
		private static void Move (GameObject from, GameObject to)
		{
			// BUG: Don't place methods with optional parameters within MonoBehaviour
			// ReSharper disable IntroduceOptionalParameters.Local
			Move(from, to, null);
			// ReSharper restore IntroduceOptionalParameters.Local
		}

		/// <summary>
		/// Navigate from one point to another.
		/// </summary>
		/// <param name="from">The outgoing point.</param>
		/// <param name="to">The incoming point.</param>
		/// <param name="path">The return path to outgoing point.</param>
		private static void Move (GameObject from, GameObject to, GameObject path)
		{
			//Debug.Log("Move From: " + from + " To: " + to + " Path: " + path);
			if (from == null || to == null) return;

			if (path != null) History.Push(path);
			SetActivate(from, false);
			SetActivate(to, true);
		}

		/// <summary>
		/// Set the active state of the target gameobject.
		/// </summary>
		/// <param name="target">The gameobject which state to change.</param>
		/// <param name="state">The state of the object.</param>
		private static void SetActivate (GameObject target, bool state)
		{
			// Tru activate root
			var mPoint = target.GetComponent<UINavigator>();
			if (mPoint != null)
			{
				// Execute custom logic
				if (state) mPoint.ExecuteEnter();
				else mPoint.ExecuteExit();

				NGUITools.SetActive(mPoint.root, state);
			}

			// Make sure to activate target
			if (NGUITools.GetActive(target) != state)
				NGUITools.SetActive(target, state);
		}

		/// <summary>
		/// This object will be activated to return this menu.
		/// </summary>
		public GameObject root;

		/// <summary>
		/// The next navigation point.
		/// </summary>
		public GameObject next;

		/// <summary>
		/// The return navigation point.
		/// </summary>
		public GameObject back;

		/// <summary>
		/// The required UI event to invoke <see cref="GoNext"/>.
		/// </summary>
		public UIEventType nextMenuEvent = UIEventType.None;

		/// <summary>
		/// The required UI event to invoke <see cref="GoBack"/>.
		/// </summary>
		public UIEventType backMenuEvent = UIEventType.None;

		/// <summary>
		/// The collection of navigation relations.
		/// </summary>
		public List<Relation> relations = new List<Relation>();

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			if (root == null) root = gameObject;
			UIEventTrace.Get(gameObject).EventTraced += OnEventTraced;
		}

		/// <summary>
		/// Move to the next menu specified in <see cref="next"/>.
		/// </summary>
		public void GoNext ()
		{
			Move(gameObject, next, back);
		}

		/// <summary>
		/// Move to the previous menu.
		/// </summary>
		public void GoBack ()
		{
			Move(gameObject, GetLastPoint());
		}

		/// <summary>
		/// This callback is raised when moving to next point.
		/// </summary>
		protected virtual void OnExit ()
		{

		}

		/// <summary>
		/// This callback is raised after entering this point.
		/// </summary>
		protected virtual void OnEnter ()
		{

		}

		/// <summary>
		/// Execute entering logic.
		/// </summary>
		private void ExecuteEnter ()
		{
			// Raise callback
			OnEnter();

			// Send messages
			foreach (var mRelation in relations)
				if (mRelation.eventType == NavigationEvent.OnEnter)
					mRelation.message.Send();
		}

		/// <summary>
		/// Execute exiting logic.
		/// </summary>
		private void ExecuteExit ()
		{
			// Raise callback
			OnExit();

			// Send messages
			foreach (var mRelation in relations)
				if (mRelation.eventType == NavigationEvent.OnExit)
					mRelation.message.Send();
		}

		/// <summary>
		/// Event trace callback.
		/// </summary>
		private void OnEventTraced (UIEventTrace sender, UIEventType eventType)
		{
			if (eventType == UIEventType.None) return;
			if (eventType == nextMenuEvent) GoNext();
			if (eventType == backMenuEvent) GoBack();
		}

		/// <summary>
		/// The type of the navigation event.
		/// </summary>
		public enum NavigationEvent
		{
			/// <summary>
			/// The event is not specified.
			/// </summary>
			None,

			/// <summary>
			/// The event on entering current point.
			/// </summary>
			OnEnter,

			/// <summary>
			/// The event on leaving current point.
			/// </summary>
			OnExit,
		}

		[Serializable]
		public class Relation
		{
			/// <summary>
			/// The type of the navigation event.
			/// </summary>
			public NavigationEvent eventType = NavigationEvent.None;

			/// <summary>
			/// The message to sent.
			/// </summary>
			public Message message;
		}
	}
}
