//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// This script will center the UIPanel over the specified point or closest transform.
	/// </summary>
	[AddComponentMenu("RNGUI/Control/Panel Centering")]
	public class UIPanelCenter : MonoBehaviour
	{
		private UIPanel mPanel;
		private UIDraggablePanel mDraggablePanel;

		/// <summary>
		/// The root transform which is used to get closest transform.
		/// </summary>
		public Transform root;

		/// <summary>
		/// The strength of the spring.
		/// </summary>
		public float springStrength = 8f;

		/// <summary>
		/// Apply reposition on next frame.
		/// </summary>
		public bool repositionNow;

		/// <summary>
		/// The last transform used as closest transform to reposition panel.
		/// </summary>
		public Transform lastClosest;

		/// <summary>
		/// This message will be after end of the reposition.
		/// </summary>
		public Message onRepositioned = new Message { function = "OnRepositioned" };

		/// <summary>
		/// Whether to use last closest object as receiver.
		/// </summary>
		public bool lastReceiver = false;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			onRepositioned.SetDefaultReceiver(gameObject);
			mPanel = NGUITools.FindInParents<UIPanel>(gameObject);

			if (mPanel != null)
				mDraggablePanel = mPanel.GetComponent<UIDraggablePanel>();

			if (root == null)
				root = transform;

			if (mPanel == null)
				Debug.LogWarning("UIPanel is not found on: " + NGUITools.GetHierarchy(gameObject));
		}

		/// <summary>
		/// Update is called every frame, if the MonoBehaviour is enabled.
		/// </summary>
		public void Update ()
		{
			if (!repositionNow) return;
			repositionNow = false;
			Reposition();
		}

		/// <summary>
		/// Reposition the panel on the the closest transform position.
		/// </summary>
		public void Reposition () { Reposition(GetClosest()); }

		/// <summary>
		/// Reposition the panel on the specified transform position.
		/// </summary>
		/// <param name="target"></param>
		public void Reposition (Transform target)
		{
			if (target == null) return;
			lastClosest = target;
			Reposition(target.position);
		}

		/// <summary>
		/// Reposition the panel so that specified point will be positioned on the center.
		/// </summary>
		/// <param name="point">The point of the world in the global coordinates.</param>
		public void Reposition (Vector3 point)
		{
			if (mPanel == null) return;
			var mTransform = mPanel.transform;
			var mPanelCenter = mTransform.InverseTransformPoint(mPanel.GetCenterPoint());
			var mPoint = mTransform.InverseTransformPoint(point);
			var mDelta = mPanelCenter - mPoint;

			if (mDraggablePanel != null)
			{
				// Avoid using blocking scale
				var mScale = mDraggablePanel.scale;
				if (mScale.x <= 0) mDelta.x = 0;
				if (mScale.y <= 0) mDelta.y = 0;
				if (mScale.z <= 0) mDelta.z = 0;
			}

			MovePanel(mDelta);
		}

		/// <summary>
		/// Finds closest transform relatively to the current transform.
		/// </summary>
		public Transform GetClosest ()
		{
			Transform mRetVal = null;
			var mMinimal = float.MaxValue;

			// Check if root not removed
			if (root == null) root = transform;

			// Cache center position
			var mCenter = mPanel.GetCenterPoint();

			foreach (Transform mChild in root)
			{
				var mDistance = Vector3.SqrMagnitude(mCenter - mChild.position);

				// Skip all instances on the same or greater distance
				if (mDistance >= mMinimal) continue;

				// Save closest transform
				mMinimal = mDistance;
				mRetVal = mChild;
			}

			return mRetVal;
		}

		/// <summary>
		/// Move the panel to the specified global point.
		/// </summary>
		/// <param name="delta">The delta position to move panel.</param>
		private void MovePanel (Vector3 delta)
		{
			if (springStrength <= 0)
			{
				if (mDraggablePanel != null)
					mDraggablePanel.MoveRelative(delta);
				else
					MoveRelative(delta);

				// Callback
				OnRepositioned();
			}
			else
			{
				var mPoint = mPanel.transform.localPosition + delta;
				SpringPanel.Begin(mPanel.gameObject, mPoint, springStrength).onFinished += OnRepositioned;
			}
		}

		/// <summary>
		/// Move the panel by the specified relative amount.
		/// </summary>
		/// <param name="delta">The delta position to move panel.</param>
		public void MoveRelative (Vector3 delta)
		{
			var mTransform = mPanel.transform;

			// Move transform
			mTransform.localPosition += delta;

			// Update clip range
			var mClipRange = mPanel.clipRange;
			mClipRange.x -= delta.x;
			mClipRange.y -= delta.y;
			mPanel.clipRange = mClipRange;
		}

		/// <summary>
		/// Callback after reposition operation.
		/// </summary>
		private void OnRepositioned ()
		{
			if (onRepositioned == null) return;

			// Check if need to use last closest gameobject
			if (lastReceiver) onRepositioned.Send(lastClosest.gameObject);
			else onRepositioned.Send();
		}
	}
}
