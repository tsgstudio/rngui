//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// The control of the <see cref="UIWindow"/>.
	/// </summary>
	public class UIWindowControl : MonoBehaviour
	{
		/// <summary>
		/// The required event to execute <see cref="action"/>.
		/// </summary>
		public UIEventType eventType = UIEventType.None;

		/// <summary>
		/// The action to execute towards the <see cref="window"/>.
		/// </summary>
		public WindowAction action = WindowAction.None;

		/// <summary>
		/// The name of the target window.
		/// </summary>
		public string windowName;

		/// <summary>
		/// The instance of the target window.
		/// </summary>
		public UIWindow window;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public virtual void Awake ()
		{
			UIEventTrace.Get(gameObject).EventTraced += OnEventTraced;
		}

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		public virtual void OnEnable ()
		{
			if (eventType == UIEventType.OnEnable)
				ExecuteAction(window ?? UIWindow.GetWindow(windowName), action);
		}

		/// <summary>
		/// The event trace callback.
		/// </summary>
		private void OnEventTraced (UIEventTrace sender, UIEventType pEventType)
		{
			if (pEventType == UIEventType.None) return;
			if (eventType != pEventType) return;

			ExecuteAction(window ?? UIWindow.GetWindow(windowName), action);
		}

		/// <summary>
		/// Execute specified action towards specified window.
		/// </summary>
		/// <param name="pWindow">The affected window.</param>
		/// <param name="pAction">The action to execute.</param>
		private static void ExecuteAction (UIWindow pWindow, WindowAction pAction)
		{
			if (pWindow != null)
				pWindow.ExecuteAction(pAction);
		}
	}
}

