//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// The component for displaying <see cref="UISlider"/> progress as text.
	/// </summary>
	[ExecuteInEditMode]
	[RequireComponent(typeof(UISlider))]
	public class UISliderText : MonoBehaviour
	{
		private UISlider _slider;

		/// <summary>
		/// The label to display slider progress.
		/// </summary>
		public UILabel label;

		/// <summary>
		/// The displaying progress format.
		/// </summary>
		public string format = "{0:0} %";

		/// <summary>
		/// Whether to calculate value in percents.
		/// </summary>
		public bool percents = true;

		/// <summary>
		/// Whether text label is dirty.
		/// </summary>
		public bool isDirty = true;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			if (_slider == null)
				_slider = GetComponent<UISlider>();

			if (_slider != null)
				_slider.onValueChange += x => isDirty = true;
		}

		/// <summary>
		/// Start is called just before any of the Update methods is called the first time.
		/// </summary>
		public void Start ()
		{
			isDirty = true;
		}

		/// <summary>
		/// LateUpdate is called every frame, if the Behaviour is enabled.
		/// </summary>
		public void LateUpdate ()
		{
			if (isDirty)
			{
				if (label != null)
				{
					var mValue = _slider.sliderValue;
					label.text = string.Format(format, percents ? mValue * 100 : mValue);
				}
				isDirty = false;
			}
		}
	}
}
