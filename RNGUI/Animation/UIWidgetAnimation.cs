//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// The script with exposed widget properties make it possible to animate widgets. 
	/// </summary>
	[ExecuteInEditMode]
	public class UIWidgetAnimation : MonoBehaviour
	{
		private Color _color;

		/// <summary>
		/// The target widget to animate.
		/// </summary>
		public UIWidget widget;

		/// <summary>
		/// The animateable color.
		/// </summary>
		public Color color = Color.white;

		/// <summary>
		/// Whether to reset values after aniamtion complete.
		/// </summary>
		public bool revertChanges = true;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public virtual void Awake ()
		{
			if (widget == null)
				widget = GetComponent<UIWidget>();

			if (widget == null) return;

			color = widget.color;
		}

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		public virtual void OnEnable ()
		{
			if (widget == null)
			{
				enabled = false;
			}
			else
			{
				_color = widget.color;
			}

		}

		/// <summary>
		/// This function is called when the behaviour becomes disabled () or inactive.
		/// </summary>
		public virtual void OnDisable ()
		{
			if (revertChanges)
				Revert();
		}

		/// <summary>
		/// Update is called every frame, if the MonoBehaviour is enabled.
		/// </summary>
		public virtual void Update ()
		{
			if (widget == null) return;

			widget.color = color;
		}

		/// <summary>
		/// Revert widget settings to the initial.
		/// </summary>
		public virtual void Revert ()
		{
			if (widget == null) return;

			widget.color = _color;
		}
	}
}
