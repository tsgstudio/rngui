//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	/// <summary>
	/// This script is used to display text from cycle buffer.
	/// </summary>
	public class UICycleText : MonoBehaviour
	{
		/// <summary>
		/// The label used to display cycle text buffer.
		/// </summary>
		public UILabel label;

		/// <summary>
		/// The collection of text to display.
		/// </summary>
		public List<string> buffer = new List<string>();

		/// <summary>
		/// The delay before next buffer element.
		/// </summary>
		public float secondsDelay = 0.5f;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			if (label == null) label = GetComponent<UILabel>();
		}

		/// <summary>
		/// This function is called when the object becomes enabled and active.
		/// </summary>
		public void OnEnable ()
		{
			StartCoroutine(CycleTextCoroutine());
		}

		/// <summary>
		/// Coroutine to cycle throught the buffer.
		/// </summary>
		/// <returns></returns>
		private IEnumerator CycleTextCoroutine ()
		{
			int i = 0;

			while (enabled)
			{
				YieldInstruction mYield;
				if (secondsDelay <= 0)
					mYield = new WaitForEndOfFrame();
				else
					mYield = new WaitForSeconds(secondsDelay);

				if (label != null)
				{
					i = i % buffer.Count;
					label.text = buffer[i++];
				}

				yield return mYield;
			}
		}
	}
}

