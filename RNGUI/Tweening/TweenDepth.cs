﻿//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;

	/// <summary>
	/// Tween the object's depth.
	/// </summary>
	[AddComponentMenu("RNGUI/Tween/Depth")]
	public class TweenDepth : UITweener
	{
		/// <summary>
		/// Initial depth.
		/// </summary>
		public int from = 0;

		/// <summary>
		/// Final depth.
		/// </summary>
		public int to = 0;

		UIWidget mWidget;

		/// <summary>
		/// Current depth.
		/// </summary>
		public int Depth
		{
			get { return mWidget != null ? mWidget.depth : 0; }
			set { if (mWidget != null) mWidget.depth = value; }
		}

		/// <summary>
		/// Find all required components.
		/// </summary>
		void Awake ()
		{
			mWidget = GetComponentInChildren<UIWidget>();
		}

		/// <summary>
		/// Actual tweening logic should go here.
		/// </summary>
		protected override void OnUpdate (float factor, bool isFinished)
		{
			Depth = Mathf.FloorToInt(from * (1f - factor) + to * factor);
		}

		/// <summary>
		/// Start the tweening operation.
		/// </summary>
		static public TweenDepth Begin (GameObject go, float duration, int depth)
		{
			TweenDepth mComp = UITweener.Begin<TweenDepth>(go, duration);
			mComp.from = mComp.Depth;
			mComp.to = depth;

			if (duration <= 0f)
			{
				mComp.Sample(1f, true);
				mComp.enabled = false;
			}
			return mComp;
		}
	}
}
