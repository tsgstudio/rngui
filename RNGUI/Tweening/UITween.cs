//----------------------------------------------
//         Copyright © 2013 TSG Studio          
//       Website: http://www.tsgstudio.com      
//----------------------------------------------
namespace RNGUI
{
	using UnityEngine;
	using AnimationOrTween;

	/// <summary>
	/// This script can activate tweeners components on remote objects.
	/// </summary>
	public class UITween : MonoBehaviour
	{
		private UITweener[] mTweeners;

		/// <summary>
		/// This event is raised when tweening have been finished.
		/// </summary>
		public event Event<UITween> Finished;

		/// <summary>
		/// This event is raised when tweening have been started.
		/// </summary>
		public event Event<UITween> Started;

		/// <summary>
		/// The target gameobject containing tweeners.
		/// </summary>
		public GameObject tweenTarget;

		/// <summary>
		/// The tweeners with different group will be ignored by this tween.
		/// </summary>
		public int tweenGroup = 0;

		/// <summary>
		/// The tween play direction.
		/// </summary>
		public Direction direction = Direction.Forward;

		/// <summary>
		/// Whether the last play direction was forward;
		/// </summary>
		public bool isLastForward;

		/// <summary>
		/// Whether to reset state of the tweener to initial before play.
		/// </summary>
		public bool resetOnPlay = false;

		/// <summary>
		/// What to do if the tweenTarget game object is currently disabled.
		/// </summary>
		public EnableCondition enableOnPlay = EnableCondition.DoNothing;

		/// <summary>
		/// What to do with the tweenTarget after the tween finishes.
		/// </summary>
		public DisableCondition disableOnFinished = DisableCondition.DoNotDisable;

		/// <summary>
		/// Whether the tweens on the child game objects will be considered.
		/// </summary>
		public bool includeChildren = false;

		/// <summary>
		/// Execute the <see cref="Play()"/> in next update.
		/// </summary>
		public bool playNow;

		/// <summary>
		/// The message to send when tween is playing forward.
		/// </summary>
		public Message onPlayForward = new Message() { function = "OnPlayForward" };

		/// <summary>
		/// The message to send when tween is playing backward.
		/// </summary>
		public Message onPlayBackward = new Message() { function = "OnPlayBackward" };

		/// <summary>
		/// Activate the tweeners in the same direction as <see cref="direction"/>.
		/// </summary>
		public void Play ()
		{
			// Check play direction
			var mForward = direction == Direction.Toggle ? !isLastForward : direction == Direction.Forward;

			ExecutePlay(mForward);
		}

		/// <summary>
		/// Activate the tweeners in specified direction.
		/// Consider that play direction will be changed depending on selected <see cref="direction"/>.
		/// When <see cref="Direction.Toggle"/> direction selected, the <paramref name="forward"/> value is ignored.
		/// </summary>
		public void Play (bool forward)
		{
			// Check play direction
			var mForward = direction == Direction.Toggle ? !isLastForward
						 : direction == Direction.Reverse ? !forward
						 : forward;

			ExecutePlay(mForward);
		}

		/// <summary>
		/// Activate the tweeners in forward direction bypassing <see cref="direction"/> settings.
		/// </summary>
		public void Play (bool forward, bool ignoreDirection)
		{
			if (ignoreDirection)
				ExecutePlay(forward);
			else
				Play(forward);
		}

		/// <summary>
		/// Activate the tweeners in forward direction bypassing <see cref="direction"/> settings.
		/// </summary>
		public void PlayForward ()
		{
			ExecutePlay(true);
		}

		/// <summary>
		/// Activate the tweeners in backward direction bypassing <see cref="direction"/> settings.
		/// </summary>
		public void PlayBackward ()
		{
			ExecutePlay(false);
		}

		/// <summary>
		/// Execute the tweeners activation.
		/// </summary>
		private void ExecutePlay (bool forward)
		{
			// Send notifiaction messages
			var mMessage = forward ? onPlayForward : onPlayBackward;
			if (mMessage != null) mMessage.Send();

			isLastForward = forward;
			var mGameObject = tweenTarget ?? gameObject;

			if (!NGUITools.GetActive(mGameObject))
			{
				// Do nothing if disabled and dont need to be enabled.
				if (enableOnPlay != EnableCondition.EnableThenPlay)
					return;

				// Enable object
				NGUITools.SetActive(mGameObject, true);
			}

			// Get the tweening components
			mTweeners = includeChildren ? mGameObject.GetComponentsInChildren<UITweener>() : mGameObject.GetComponents<UITweener>();

			if (mTweeners.Length == 0)
			{
				// Check disable condition
				var mRequireDisable = (disableOnFinished == DisableCondition.DisableAfterForward && forward) ||
									  (disableOnFinished == DisableCondition.DisableAfterReverse && !forward);

				// Disable if required
				if (mRequireDisable) NGUITools.SetActive(tweenTarget, false);
			}
			else
			{
				// Activate each tweener
				foreach (var mTweener in mTweeners)
				{
					// Skip tweens with different group
					if (mTweener.tweenGroup != tweenGroup) continue;

					// Ensure that the game objects are enabled
					if (!NGUITools.GetActive(mGameObject)) NGUITools.SetActive(mGameObject, true);

					// Play the tweener
					mTweener.Play(forward);

					// Reset tweener
					if (resetOnPlay) mTweener.Reset();

					// Set callbacks
					mTweener.onFinished += OnTweenerFinished;
				}

				// Raise event
				OnStarted();
			}
		}

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		public void Awake ()
		{
			if (tweenTarget == null) tweenTarget = gameObject;
			onPlayForward.SetDefaultReceiver(gameObject);
			onPlayBackward.SetDefaultReceiver(gameObject);
		}

		/// <summary>
		/// Update is called every frame, if the MonoBehaviour is enabled.
		/// </summary>
		public void Update ()
		{
			if (mTweeners == null) return;

			var mHasFinished = true;
			var mSameDirection = true;

			foreach (var mTweener in mTweeners)
			{
				// Skip all tweeners with different group
				if (mTweener.tweenGroup != tweenGroup) continue;

				// Check if finished
				mHasFinished = !mTweener.enabled;

				// Skip frame if any of the the tweeners is still enabled
				if (!mHasFinished) break;

				// All tweeners must be in same direction
				if ((int)mTweener.direction != (int)disableOnFinished)
					mSameDirection = false;
			}

			// Skip frame if not finished
			if (!mHasFinished) return;

			// Raise event
			OnFinished();

			// Release tweeners
			mTweeners = null;

			// Do not disable if not met conditions
			if (disableOnFinished == DisableCondition.DoNotDisable) return;

			// Disable target
			if (mSameDirection) NGUITools.SetActive(tweenTarget, false);
		}

		/// <summary>
		/// LateUpdate is called every frame, if the Behaviour is enabled.
		/// </summary>
		public void LateUpdate ()
		{
			if (playNow)
			{
				playNow = false;
				Play();
			}
		}

		/// <summary>
		/// Raise <see cref="Finished"/> event.
		/// </summary>
		private void OnFinished ()
		{
			if (Finished != null) Finished(this);
		}

		/// <summary>
		/// Raise <see cref="Started"/> event.
		/// </summary>
		private void OnStarted ()
		{
			if (Started != null) Started(this);
		}

		/// <summary>
		/// Callback for each tweener.
		/// </summary>
		private void OnTweenerFinished (UITweener tween)
		{
			// Unsuscribe event
			tween.onFinished -= OnTweenerFinished;

			// Chek if all finished
			if (mTweeners == null)
				OnFinished();
		}
	}
}
